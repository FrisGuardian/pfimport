Hooks.on("getCompendiumDirectoryEntryContext", (html, list) => {
    console.log(html, list);
    list.push({
        name: "JSON Import",
        icon: "<i class='fas fa-file-import'></i>",
        callback: li => {
            PFImporter.Prompt(game.packs.find(p => p.collection === li.attr("data-pack")))
        }
    })
})

class PFImporter {

// CONFIG.debug.hooks = true
static Prompt(compendium) 
{
    let html = 
    `
    <label for="option">Item Type:</label>
    <select id="option">
        <option value="alchemical">Alchemical</option>
        <option value="magicweapon">Magic Weapon</option>
        <option value="wand">Wand</option>
    </select><br><br>
    <textarea class = "pf-import-text" name= "importData"></textarea>`
    new Dialog({
        title: "JSON Import",
        content : html,
        buttons : {
            confirm : {
                label: "Confirm",
                callback : (html) => this.PFImport(compendium, JSON.parse(html.find('[name="importData').val()), html.find('[id="option"]').val())
            },
            cancel : {
                label: "Cancel"
            }
        },
        default: "confirm"
    }).render(true);
}

static PFImport(compendium, file, type)
{
    if(type === "alchemical"){
        this.getBombs(compendium,file);
        this.getElixers(compendium,file);
        this.getPoisons(compendium,file);
        this.getTools(compendium, file);
    }
    if(type === "wand"){
        this.getWands(compendium, file);
    }
    if(type === "magicweapon"){
        this.getMagicWeapons(compendium, file);
    }
    
    
}

static getBombs(compendium,file){
    let bombs = file.bombs;
    
    for(let index in bombs){
        if (!isNaN(index)){
            let actor = null;
            let item = ItemPF2e.create({name: bombs[index].name, type:"weapon"},{temporary: true, displaySheet: false}).then( actor => {
                //var re = /(([1-9])d([1-9]))/;
                //var array = re.exec(bomb.text);
                actor.data.data.description.value = bombs[index].text;
                actor.data.data.level.value = bombs[index].level;
                actor.data.data.price.value = bombs[index].price;
                actor.data.data.group.value = "Bomb";
                actor.data.data.weight.value = bombs[index].bulk
                actor.data.data.range.value = "20 ft."
                compendium.createEntity(actor);
            })
            
        }
    }
}

static getElixers(compendium, file){
    let elixir = file.elixirs;

    for(let index in elixir){
        if (!isNaN(index)){
            let item = ItemPF2e.create({name: elixir[index].name, type:"consumable"},{temporary: true, displaySheet: false}).then( actor => {
                actor.data.data.description.value = elixir[index].text;
                actor.data.data.level.value = elixir[index].level;
                actor.data.data.price.value = elixir[index].price;
                actor.data.data.consumableType.value = "potion";
                actor.data.data.weight.value = elixir[index].bulk
                compendium.createEntity(actor);
            })
            
        }
    }

}

static getPoisons(compendium, file){
    let poison = file.poisons;

    for(let index in poison){
        if (!isNaN(index)){
            let item = ItemPF2e.create({name: poison[index].name, type:"consumable"},{temporary: true, displaySheet: false}).then( actor => {
                actor.data.data.description.value = poison[index].text;
                actor.data.data.level.value = poison[index].level;
                actor.data.data.price.value = poison[index].price;
                actor.data.data.consumableType.value = "potion";
                actor.data.data.weight.value = poison[index].bulk
                compendium.createEntity(actor);
            })
            
        }
    }

}

static getTools(compendium,file){
    let tool = file.tools;
    for(let index in tool){
        if (!isNaN(index)){
            let item = ItemPF2e.create({name: tool[index].name, type:"consumable"},{temporary: true, displaySheet: false}).then( actor => {
                actor.data.data.description.value = tool[index].text;
                actor.data.data.level.value = tool[index].level;
                actor.data.data.price.value = tool[index].price;
                actor.data.data.consumableType.value = "other";
                actor.data.data.weight.value = tool[index].bulk
                compendium.createEntity(actor);
            })
            
        }
    }

}

static getMagicWeapons(compendium,file){
    let items = file.magicWeapons;
    for(let index in items){
        if (!isNaN(index)){
            let p = ItemPF2e.create({name: items[index].name, type:"weapon"},{temporary: true, displaySheet: false}).then( item => {
                let effect = null;
                try{ effect = items[index].effect;}
                catch(err){ effect = ""}
                item.data.data.description.value = items[index].text + "\n" + effect;
                item.data.data.level.value = items[index].level;
                item.data.data.price.value = items[index].price;
                item.data.data.weight.value = items[index].bulk
                compendium.createEntity(item);
            })
            
        }
    }

}

static getWands(compendium,file){
    let items = file.wands;
    for(let index in items){
        if (!isNaN(index)){
            let p = ItemPF2e.create({name: items[index].name, type:"weapon"},{temporary: true, displaySheet: false}).then( item => {
                let effect = null;
                try{ effect = items[index].effect;}
                catch(err){ effect = ""}
                item.data.data.description.value = items[index].text + "\n" + effect;
                item.data.data.level.value = items[index].level;
                item.data.data.price.value = items[index].price;
                item.data.data.weight.value = items[index].bulk
                compendium.createEntity(item);
            })
            
        }
    }

}

}